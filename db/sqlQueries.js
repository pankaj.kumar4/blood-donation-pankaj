const mysql = require('mysql');
require('dotenv').config();

const mysqlConnection = mysql.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: process.env.database,
});
mysqlConnection.connect((err) => {
  if (err) throw err;
});


const sqlQuery = (query, parameters = []) => new Promise((resolve, reject) => {
  mysqlConnection.query(query, parameters, (err, rows) => {
    if (!err) {
      resolve(rows);
    } else {
      reject(err);
    }
  });
});

function createNewUser(email, password) {
  return sqlQuery('INSERT INTO users(email, password) values(?,?);', [email, password]);
}

function fetchNewUser(email) {
  return sqlQuery('SELECT id,email FROM users where email=?', [email]);
}
function fetchPassword(email) {
  return sqlQuery('select password FROM users WHERE email=? ', [email]);
}
function fetchVideo() {
  return sqlQuery('SELECT * FROM video');
}

function fetchVideoById(id) {
  return sqlQuery('SELECT * FROM video WHERE id=?', [id]);
}
function fetchVideoByUrl(url) {
  return sqlQuery('SELECT * FROM video WHERE url=?', [url]);
}

function uploadNewVideo(userId, url) {
  return sqlQuery('INSERT INTO video(url,user_id) VALUES(?,?)', [url, userId]);
}

function updateVideo(url, userId, id) {
  return sqlQuery('UPDATE video SET user_id=?,url=? WHERE id=?', [userId, url, id]);
}

function deleteVideo(id) {
  return sqlQuery('DELETE FROM video WHERE id=?', [id]);
}
module.exports = {
  fetchVideo,
  uploadNewVideo,
  updateVideo,
  deleteVideo,
  fetchVideoById,
  createNewUser,
  fetchPassword,
  fetchNewUser,
  fetchVideoByUrl,
};
