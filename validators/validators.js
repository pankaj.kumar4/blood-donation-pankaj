const Joi = require('joi');

function validateId(id) {
  const schema = Joi.object({
    id: Joi.number().integer().required(),
  });
  const res = Joi.validate({ id }, schema);
  return res;
}

function validateRegister(email, password) {
  const schema = Joi.object({
    email: Joi.string().email(),
    password: Joi.string().min(3).max(15).required(),
  });
  return Joi.validate({ email, password }, schema);
}

function validateLogin(id, email, password) {
  const schema = Joi.object({
    id: Joi.number().integer().required(),
    email: Joi.string().email(),
    password: Joi.string().min(3).max(15).required(),
  });
  return Joi.validate({ id, email, password }, schema);
}

function validateVideo(url, userId, id) {
  const schema = Joi.object({
    url: Joi.string().regex(/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/),
    id: Joi.number().integer().required(),
    user_id: Joi.number().integer(),
  });
  return Joi.validate({ url, user_id: userId, id }, schema);
}

function validateNewVideo(url, userId) {
  const schema = Joi.object({
    url: Joi.string().regex(/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/),
    user_id: Joi.number().integer(),
  });
  return Joi.validate({ url, user_id: userId }, schema);
}
module.exports = {
  validateId,
  validateVideo,
  validateRegister,
  validateLogin,
  validateNewVideo,
};
