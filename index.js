const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const createError = require('./middleware/error');
const videos = require('./routes/api/videos');
const singUp = require('./routes/api/signup');
const verifyToken = require('./middleware/authentication');

const app = express();

app.use(bodyParser.json());
app.use(verifyToken);
// Handlebars
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

// routes

app.use('/', videos);
app.use('/', singUp);
app.use(createError);
const PORT = process.env.PORT || 4000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
