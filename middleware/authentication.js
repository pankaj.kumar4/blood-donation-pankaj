const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  if (req.method === 'GET') {
    next();
  } else {
    const bearerHeader = req.headers.authorization;
    if (typeof bearerHeader !== 'undefined') {
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      try {
        const realUser = jwt.verify(bearerToken, process.env.secretKey);
        next();
      } catch (error) { res.status(403).send({ msg: 'Invalid Token' }); }
    } else {
      res.sendStatus(403);
    }
  }
}
module.exports = verifyToken;
