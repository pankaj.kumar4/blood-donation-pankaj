function errorhandler(error, req, res, next) {
  res.json(error);
  next();
}
module.exports = errorhandler;
