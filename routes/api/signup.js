const express = require('express');
const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const bcrypt = require('bcryptjs');
const validators = require('../../validators/validators');
const db = require('../../db/sqlQueries');

const router = express.Router();
router.post('/register', (req, res, next) => {
  const { error } = validators.validateRegister(req.body.email, req.body.password);
  if (error !== null) {
    next(createError(400));
  }
  const salt = bcrypt.genSaltSync(10);
  const { email } = req.body;
  const password = bcrypt.hashSync(req.body.password, salt);
  db.createNewUser(email, password)
    .then(
      db.fetchNewUser(email)
        .then((user) => res.send(user)),
    )
    .catch(next(createError(400)));
});


router.post('/login', (req, res, next) => {
  const { error } = validators.validateRegister(req.id, req.body.email, req.body.password);
  if (error !== null) {
    next(createError(400));
  }
  const { id } = req.body;
  const { email } = req.body;
  const { password } = req.body;
  db.fetchPassword(email)
    .then((userPassword) => {
      const checkPassword = bcrypt.compareSync(password, userPassword[0].password);
      if (!checkPassword) {
        next(createError(401));
      }
      const expireTime = 24 * 60 * 60;
      const accessToken = jwt.sign({ id }, process.env.secretKey, { expiresIn: expireTime });
      res.send({ userId: id, accessToken, expiresIn: expireTime });
    })
    .catch(next(createError(500)));
});

module.exports = router;
