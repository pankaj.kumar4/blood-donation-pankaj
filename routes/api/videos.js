const express = require('express');
const createError = require('http-errors');
const db = require('../../db/sqlQueries');
const validators = require('../../validators/validators');

const router = express.Router();
router.get('/videos', (req, res, next) => {
  db.fetchVideo()
    .then((rows) => {
      if (rows.length === 0) {
        next(createError(404));
      }
      if (req.headers['content-type'] === 'application/json') {
        res.json(rows);
      }
      if (req.headers['content-type'] === 'text/html') {
        res.render('videos', { data: rows, title: 'Videos' });
      }
    })
    .catch(createError(500));
});

router.get('/videos/:id', (req, res, next) => {
  const { error } = validators.validateId(req.params.id);
  if (error !== null) {
    next(createError(400));
  }
  db.fetchVideoById(req.params.id)
    .then((rows) => {
      if (rows.length === 0) {
        next(createError(404));
      }
      if (req.headers['content-type'] === 'application/json') {
        res.json(rows);
      }
      if (req.headers['content-type'] === 'text/html') {
        res.render('videos', {
          data: rows,
          title: `Video By Id:${req.params.id}`,
        });
      }
    })
    .catch(() => {
      next(createError(500));
    });
});

router.post('/videos', (req, res, next) => {
  const newVideo = {
    user_id: req.body.user_id,
    url: req.body.url,
  };

  const { error } = validators.validateNewVideo(req.body.url, req.body.user_id);
  if (error !== null) {
    next(createError(400));
  }
  db.uploadNewVideo(newVideo.user_id, newVideo.url).then(
    db.fetchVideoByUrl(req.body.url).then((rows) => {
      if (req.headers['content-type'] === 'application/json') {
        res.json(rows);
      }
      if (req.headers['content-type'] === 'text/html') {
        res.render('videos', { data: rows, title: 'Videos' });
      }
    }),
  );
});

router.put('/videos/:id', (req, res, next) => {
  const { error } = validators.validateVideo(
    req.body.url,
    req.body.user_id,
    req.body.id,
  );
  if (error !== null) {
    next(createError(400));
  }
  db.fetchVideoById(req.body.id).then(
    (rows) => {
      if (rows.length === 0) {
        next(createError(404));
      }
    },
  );
  db.updateVideo(req.body.url, req.body.user_id, req.body.id).then(
    db.fetchVideoById(req.body.id).then((rows) => {
      if (req.headers['content-type'] === 'application/json') {
        res.json(rows);
      }
      if (req.headers['content-type'] === 'text/html') {
        res.render('videos', { data: rows, title: 'Videos' });
      }
    }),
  );
});

router.delete('/videos/:id', (req, res, next) => {
  const { error } = validators.validateId(req.params.id);
  if (error !== null) {
    next(createError(400));
  }
  db.deleteVideo(req.params.id)
    .then((rows) => {
      if (rows.affectedRows === 0) {
        next(createError(404));
      } else {
        res.send(`Video removed by id${req.params.id}`);
      }
    })
    .catch(createError(500));
});
module.exports = router;
